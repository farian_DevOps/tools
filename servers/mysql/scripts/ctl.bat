@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop


"C:\servers\mysql\bin\mysqld" --defaults-file="C:\servers\mysql\bin\my.ini" --standalone
if errorlevel 1 goto error
goto finish

:stop
cmd.exe /C start "" /MIN call "C:\servers\killprocess.bat" "mysqld.exe"

if not exist "C:\servers\mysql\data\%computername%.pid" goto finish
echo Delete %computername%.pid ...
del "C:\servers\mysql\data\%computername%.pid"
goto finish


:error
echo MySQL could not be started

:finish
exit
