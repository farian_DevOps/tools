@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop

if exist C:\servers\hypersonic\scripts\ctl.bat (start /MIN /B C:\servers\server\hsql-sample-database\scripts\ctl.bat START)
if exist C:\servers\ingres\scripts\ctl.bat (start /MIN /B C:\servers\ingres\scripts\ctl.bat START)
if exist C:\servers\mysql\scripts\ctl.bat (start /MIN /B C:\servers\mysql\scripts\ctl.bat START)
if exist C:\servers\postgresql\scripts\ctl.bat (start /MIN /B C:\servers\postgresql\scripts\ctl.bat START)
if exist C:\servers\apache\scripts\ctl.bat (start /MIN /B C:\servers\apache\scripts\ctl.bat START)
if exist C:\servers\openoffice\scripts\ctl.bat (start /MIN /B C:\servers\openoffice\scripts\ctl.bat START)
if exist C:\servers\apache-tomcat\scripts\ctl.bat (start /MIN /B C:\servers\apache-tomcat\scripts\ctl.bat START)
if exist C:\servers\resin\scripts\ctl.bat (start /MIN /B C:\servers\resin\scripts\ctl.bat START)
if exist C:\servers\jetty\scripts\ctl.bat (start /MIN /B C:\servers\jetty\scripts\ctl.bat START)
if exist C:\servers\subversion\scripts\ctl.bat (start /MIN /B C:\servers\subversion\scripts\ctl.bat START)
rem RUBY_APPLICATION_START
if exist C:\servers\lucene\scripts\ctl.bat (start /MIN /B C:\servers\lucene\scripts\ctl.bat START)
if exist C:\servers\third_application\scripts\ctl.bat (start /MIN /B C:\servers\third_application\scripts\ctl.bat START)
goto end

:stop
echo "Stopping services ..."
if exist C:\servers\third_application\scripts\ctl.bat (start /MIN /B C:\servers\third_application\scripts\ctl.bat STOP)
if exist C:\servers\lucene\scripts\ctl.bat (start /MIN /B C:\servers\lucene\scripts\ctl.bat STOP)
rem RUBY_APPLICATION_STOP
if exist C:\servers\subversion\scripts\ctl.bat (start /MIN /B C:\servers\subversion\scripts\ctl.bat STOP)
if exist C:\servers\jetty\scripts\ctl.bat (start /MIN /B C:\servers\jetty\scripts\ctl.bat STOP)
if exist C:\servers\hypersonic\scripts\ctl.bat (start /MIN /B C:\servers\server\hsql-sample-database\scripts\ctl.bat STOP)
if exist C:\servers\resin\scripts\ctl.bat (start /MIN /B C:\servers\resin\scripts\ctl.bat STOP)
if exist C:\servers\apache-tomcat\scripts\ctl.bat (start /MIN /B /WAIT C:\servers\apache-tomcat\scripts\ctl.bat STOP)
if exist C:\servers\openoffice\scripts\ctl.bat (start /MIN /B C:\servers\openoffice\scripts\ctl.bat STOP)
if exist C:\servers\apache\scripts\ctl.bat (start /MIN /B C:\servers\apache\scripts\ctl.bat STOP)
if exist C:\servers\ingres\scripts\ctl.bat (start /MIN /B C:\servers\ingres\scripts\ctl.bat STOP)
if exist C:\servers\mysql\scripts\ctl.bat (start /MIN /B C:\servers\mysql\scripts\ctl.bat STOP)
if exist C:\servers\postgresql\scripts\ctl.bat (start /MIN /B C:\servers\postgresql\scripts\ctl.bat STOP)

:end

